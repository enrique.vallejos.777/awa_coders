import React from 'react';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
const Usuario = () => {
    let usuario = "Adam Gontier"
    return ( 
        <div>
            <div>     
                <h2 className="col-md-5 offset-md-2">Usuario: <h2 className="text-primary">{usuario}</h2></h2>
                    <div className="text-center"> 

                    <div className="text-center">
            <Link to="/nuevogrupo" className="btn btn-warning">
               Nuevo grupo
           </Link>
		    <Link to="/nuevomiembro" className="btn btn-success">
                Agregar Miembro
            </Link>
            <Link to="/modificarperfil" className="btn btn-info">
               Modificar perfil        
            </Link>
            <Link to="/inicio" className="btn btn-danger">
               Cerrar sesion 
            </Link>
            </div>



                        <h1>Grupos en los que te encuentras registrados:</h1>
                        <br/>
                    </div> 
      
            <table className="table container bg-muted">
                <thead className="thead-dark bg-dark text-white">
                <tr>
      <th scope="col"WIDTH="700">#</th>
      <th scope="col" WIDTH="700" 
	    >Nombre del grupo </th>
      <th scope="col" WIDTH="700">Fecha de creacion</th>
      <th scope="col" WIDTH="700">Eliminar</th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td><a href="">Alimentos inorganicos</a></td>
      <td>25/01/2021</td>
      <td > <p className="btn btn-danger text-black ">X</p></td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td><a href="">Animes</a></td>
      <td>25/01/2021</td>
      <td > <p className="btn btn-danger text-black ">X</p></td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td><a href="">Psicopatas en la sociedad</a></td>
      <td>12/02/2021</td>
      <td > <p className="btn btn-danger text-black ">X</p></td>
    </tr>
  </tbody>
</table>
</div>
        </div>
     );
}
 
export default Usuario;