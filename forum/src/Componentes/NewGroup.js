import React from 'react';
import enconstruccion from './enconstruccion.jpg'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
const NewGroup = () => {
    return (  
        <div className="text-center"><h1 >Nuevo grupo</h1>
        <img className="img-thumbnail" src={enconstruccion} />
         <Link to="/" className="btn btn-danger">
               Atrás
            </Link>
        </div>
    );
}
 
export default NewGroup;