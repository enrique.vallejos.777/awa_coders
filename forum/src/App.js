import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Integrantes from './Componentes/Integrantes';

import NewGroup from './Componentes/NewGroup';
import Usuario from './Componentes/Usuario';
import NuevosMiebros from './Componentes/NuevoMiembro';
import Modificarperfil from './Componentes/ModificarPerfil';
import Inicio from './Componentes/Inicio';
function App() {
  let usuario = "Jose Montaño Vargas"
  return (
    <Router>
    <div className="container">
      <h1 className="text-center py-10 mt-5"><b>FORUM</b> </h1>
     </div>
     <Switch>
       <Route path="/nuevogrupo" exact>
          <NewGroup/>
       </Route>
       <Route path="/nuevomiembro" exact>
          <NuevosMiebros />
       </Route>
       <Route path="/modificarperfil" exact>
          <Modificarperfil/>
       </Route>
       <Route path="/inicio" exact>
            <Inicio />
       </Route>
       <Route path="/" exact>
          <Usuario />
       </Route>
     </Switch>
    </Router>
  );
}

export default App;
